import pandas as pd

ghh = pd.read_csv("all_values_VR_atan2_h5_ghh_b-tagging_all_jpas_reweighted.csv", low_memory=False)
qcd = pd.read_csv("all_values_VR_atan2_h5_qcd_b-tagging_all_jpas_reweighted.csv", low_memory=False)

ghh.drop(ghh.index[ghh['GhostHBosonsCount'] == 0], inplace=True)

ghh_full=pd.concat([ghh, qcd]).sample(frac=1).reset_index(drop=True)
all_variables = ghh_full[['fat_jet_pt','fat_jet_eta','fat_jet_phi','fat_jet_energy','Split12','Split23','Qw','PlanarFlow','Angularity','Aplanarity','ZCut12','KtDR','C2','D2','e3','Tau21_wta','Tau32_wta','FoxWolfram20',
                         'dipolarity','jipolarity','b_jipolarity','jetpull_angle','b_jetpull_angle',
                         'event_weight','GhostHBosonsCount']]
control = ghh_full[['fat_jet_pt','fat_jet_eta','fat_jet_phi','fat_jet_energy','Split12','Split23','Qw','PlanarFlow','Angularity','Aplanarity','ZCut12','KtDR','C2','D2','e3','Tau21_wta','Tau32_wta','FoxWolfram20',
                   'event_weight','GhostHBosonsCount']]


slice=[]
slice_control=[]


slice.append(all_variables[ghh_full['fat_jet_pt'] < 300000].sample(n=5620))

slice.append(all_variables[(ghh_full['fat_jet_pt'] > 300000) & (ghh_full['fat_jet_pt'] < 350000)].sample(n=5620))

slice.append(all_variables[(ghh_full['fat_jet_pt'] > 350000) & (ghh_full['fat_jet_pt'] < 400000)].sample(n=5620))

slice.append(all_variables[(ghh_full['fat_jet_pt'] > 400000) & (ghh_full['fat_jet_pt'] < 450000)].sample(n=5620))

slice.append(all_variables[(ghh_full['fat_jet_pt'] > 450000)])



#slice7=all_variables[(ghh_full['fat_jet_pt'] > 600000)]


for i in range(len(slice)):
    print(len(slice[i]))
    slice_control.append(slice[i][['Split12','Split23','Qw','PlanarFlow','Angularity','Aplanarity','ZCut12','KtDR','C2','D2','e3','Tau21_wta','Tau32_wta','FoxWolfram20',
                       'event_weight','GhostHBosonsCount']])
    slice_control[i].to_csv('ghh_s'+str(i+1)+'_VR_training_control.csv')
    slice[i]=slice[i][['Split12','Split23','Qw','PlanarFlow','Angularity','Aplanarity','ZCut12','KtDR','C2','D2','e3','Tau21_wta','Tau32_wta','FoxWolfram20',
                         'dipolarity','jipolarity','b_jipolarity','jetpull_angle','b_jetpull_angle',
                         'event_weight','GhostHBosonsCount']]
    slice[i].to_csv('ghh_s'+str(i+1)+'_VR_training_all_variables.csv')




all_variables = ghh_full[['Split12','Split23','Qw','PlanarFlow','Angularity','Aplanarity','ZCut12','KtDR','C2','D2','e3','Tau21_wta','Tau32_wta','FoxWolfram20',
                         'dipolarity','jipolarity','b_jipolarity','jetpull_angle','b_jetpull_angle',
                         'event_weight','GhostHBosonsCount']]
control = ghh_full[['Split12','Split23','Qw','PlanarFlow','Angularity','Aplanarity','ZCut12','KtDR','C2','D2','e3','Tau21_wta','Tau32_wta','FoxWolfram20',
                   'event_weight','GhostHBosonsCount']]
control = control.sample(frac=1).reset_index(drop=True)
all_variables = all_variables.sample(frac=1).reset_index(drop=True)

all_variables.to_csv('ghh_weighted_VR_training_all_variables.csv')
control.to_csv('ghh_weighted_VR_training_control.csv')