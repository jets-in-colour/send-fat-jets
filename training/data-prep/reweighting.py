import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys

ztt_file = "all_values_VR_atan2_ztt_b-taggin_all_jpas.csv" # Location to ztt or ghh file
qcd_file = "all_values_VR_atan2_h5_qcd_b-tagging_all_jpas.csv" #sys.argv[1] # Location to qcd file
#ztt_1 = pd.read_csv("all_values_VR_atan2_h5_ztt_b-tagging_all_jpas0.0-300.0GeV.csv", low_memory=False)
#ztt_2 = pd.read_csv("all_values_VR_atan2_h5_ztt_b-tagging_all_jpas300.0-350.0GeV.csv", low_memory=False)
#ztt_3 = pd.read_csv("all_values_VR_atan2_h5_ztt_b-tagging_all_jpas350.0-400.0GeV.csv", low_memory=False)
#ztt_4 = pd.read_csv("all_values_VR_atan2_h5_ztt_b-tagging_all_jpas400.0-450.0GeV.csv", low_memory=False)
#ztt_5 = pd.read_csv("all_values_VR_atan2_h5_ztt_b-tagging_all_jpas450.0-infGeV.csv", low_memory=False)
#ztt = pd.concat([ztt_1,ztt_2,ztt_3,ztt_4,ztt_5])
#ztt.to_csv("all_values_VR_atan2_ztt_b-taggin_all_jpas.csv",index=False)
ztt_pt, ztt_weights = np.hsplit(pd.read_csv(ztt_file).dropna()[["fat_jet_pt","event_weight"]].to_numpy(),2)
qcd_pt, qcd_weights = np.hsplit(pd.read_csv(qcd_file).dropna()[["fat_jet_pt","event_weight"]].to_numpy(),2)
ztt_pt, ztt_weights = ztt_pt.flatten(), ztt_weights.flatten()
qcd_pt, qcd_weights = qcd_pt.flatten(), qcd_weights.flatten()
#qcd_weights = np.ones(len(qcd_pt))

print(max(ztt_pt))
print(max(qcd_pt))
bins = np.linspace(min(np.hstack((qcd_pt,ztt_pt))),(max(np.hstack((qcd_pt,ztt_pt)))),20)
ztt_bins = plt.hist(ztt_pt,bins=bins,weights=ztt_weights, label="ztt",alpha=0.5)
qcd_bins = plt.hist(qcd_pt,bins=bins,weights=qcd_weights,label="qcd",alpha=0.5)

reweight_arr = np.divide(ztt_bins[0],qcd_bins[0])
max_arr_index = 0
for i in range(len(reweight_arr)):
    if (np.isnan(reweight_arr[i]) or np.isinf(reweight_arr[i])):
            max_arr_index = i
            break
reweight_arr = reweight_arr[0:max_arr_index]
max_pt = bins[max_arr_index + 1]
bins = bins[0:max_arr_index+1]

ztt_proper_pt_indices=np.where(ztt_pt > max_pt)[0]
if(len(ztt_proper_pt_indices)>0):
    ztt_weights[ztt_proper_pt_indices] = np.zeros(len(ztt_proper_pt_indices))
qcd_proper_pt_indices = np.where(qcd_pt>max_pt)[0]
if(len(qcd_proper_pt_indices) > 0):
    qcd_weights[qcd_proper_pt_indices] = np.zeros(len(qcd_proper_pt_indices))

for i in range(len(bins)-1):
    indices = np.where((qcd_pt>=bins[i]) & (qcd_pt<bins[i+1]))[0]
    qcd_weights[indices] = qcd_weights[indices]*reweight_arr[i]

plt.clf()

ztt_bins = plt.hist(ztt_pt,bins=bins,weights=ztt_weights, label="ztt",alpha=0.5)
plt.legend()
plt.show()
qcd_bins = plt.hist(qcd_pt,bins=bins,weights=qcd_weights,label="qcd",alpha=0.5)

#plt.yscale("log")
#plt.xlim([0.25e6,0.75e6])
plt.legend()
plt.show()

df_qcd = pd.read_csv(qcd_file).dropna()
df_qcd['event_weight'] = qcd_weights
df_qcd = df_qcd[df_qcd['event_weight'] != 0]
df_qcd.to_csv("all_values_VR_atan2_h5_qcd_b-tagging_all_jpas_reweighted.csv",index=False)
ztt_1 = pd.read_csv("all_values_VR_atan2_h5_ztt_b-tagging_all_jpas0.0-300.0GeV.csv", low_memory=False)
ztt_2 = pd.read_csv("all_values_VR_atan2_h5_ztt_b-tagging_all_jpas300.0-350.0GeV.csv", low_memory=False)
ztt_3 = pd.read_csv("all_values_VR_atan2_h5_ztt_b-tagging_all_jpas350.0-400.0GeV.csv", low_memory=False)
ztt_4 = pd.read_csv("all_values_VR_atan2_h5_ztt_b-tagging_all_jpas400.0-450.0GeV.csv", low_memory=False)
ztt_5 = pd.read_csv("all_values_VR_atan2_h5_ztt_b-tagging_all_jpas450.0-infGeV.csv", low_memory=False)
df_ztt = pd.concat([ztt_1,ztt_2,ztt_3,ztt_4,ztt_5]).dropna()
df_ztt['event_weight'] = ztt_weights
df_ztt = df_ztt[df_ztt['event_weight'] != 0]
df_ztt.to_csv("all_values_VR_atan2_h5_ztt_b-tagging_all_jpas_reweighted.csv",index=False)


